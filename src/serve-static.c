#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>

#include "global.h"

void serve_static(struct http_request *request){
	static char buffer[4096], *buffer_w, *file_path, *real_path;
	static FILE *file = NULL;

	file_path = get_mem(request->host->root_length + (request->version - request->target));
	sprintf(file_path, "%s%s", request->host->root, request->target);

	real_path = realpath(file_path, NULL);
	if(real_path == NULL)
		request->response.code = 404;

	else{
		if(!strncmp(request->host->root, real_path, request->host->root_length)){
			file = fopen(file_path, "r");
			if(file == NULL)
				request->response.code = 404;
		}else
			request->response.code = 403;
	}

	free(real_path);
	free_mem(file_path);

	buffer_w = &buffer[0];
	buffer_w += sprintf(buffer_w, "HTTP/1.1 %hu\r\n", request->response.code);

	if(request->response.code != 404)
		buffer_w += sprintf(buffer_w, "Content-Type:%s\r\n", request->media_type->type);

	if(file != NULL){
		fseek(file, 0, SEEK_END);
		buffer_w += sprintf(buffer_w, "Content-Length:%lu\r\n", ftell(file));
		rewind(file);

	}else if(strcmp(request->method, "HEAD") != 0){
		memcpy(buffer_w, "Content-Length:0\r\n", 18);
		buffer_w += 18;
	}

	request->responded = 1;
	memcpy(buffer_w, "\r\n", 2);
	if(send(request->socket_fd, buffer, buffer_w - buffer + 2, 0) < 0){
		if(file != NULL){
			fclose(file);
			file = NULL;
		}
		return;
	}

	if(file != NULL){
		if(strcmp(request->method, "HEAD") != 0)
			while(!feof(file)){
				static size_t read_len;

				read_len = fread(buffer, 1, 4095, file);
				send(request->socket_fd, buffer, read_len, 0);
			}

		fclose(file);
		file = NULL;
	}
}



void serve_static_media_types(){
	add_media_type("js", "application/javascript", &serve_static);
	add_media_type("ico", "image/x-icon", &serve_static);
	add_media_type("css", "text/css", &serve_static);
	add_media_type("htm", "text/html", &serve_static);
	add_media_type("html", "text/html", &serve_static);
}

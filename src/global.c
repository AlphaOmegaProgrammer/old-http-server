#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>

#include "global.h"


#define MAX_MEM_LEN_BITS 25

struct mem_pool{
	unsigned long in_use:1;
	unsigned long size:MAX_MEM_LEN_BITS;
	unsigned long :0;
	void *mem;
	struct ll *node;
};



struct ll *hosts_root = NULL, *mem_pool_root = NULL, *media_types_root = NULL;



void add_media_type(char *ext, char *type, void (*handler)(struct http_request *request)){
	static struct ll *node;
	static struct media_type *media_type;

	node = malloc(sizeof(struct ll));
	if(node == NULL)
		exit(1);

	media_type = malloc(sizeof(struct media_type));
	if(media_type == NULL)
		exit(1);

	node->next = media_types_root;
	node->data = media_type;
	media_types_root = node;

	media_type->ext = ext;
	media_type->type = type;
	media_type->handler = handler;
}

void set_request_media_type(struct http_request *request){
	static struct ll *node;

	node = media_types_root;
	while(node != NULL){
		static struct media_type *media_type;

		media_type = node->data;
		if(!strcmp(media_type->ext, request->target_ext)){
			request->media_type = media_type;
			return;
		}

		node = node->next;
	}
}



void add_host(char *hostname, char *port, char *target_base, char *root){
	static struct host *host;
	static struct ll *node;

	host = malloc(sizeof(struct host));
	if(host == NULL)
		exit(1);

	node = malloc(sizeof(struct ll));
	if(node == NULL)
		exit(1);

	host->hostname = hostname;
	host->hostname_length = strlen(hostname);
	host->port = port;
	host->target_base = target_base;
	host->target_base_length = strlen(target_base);
	host->root = root;
	host->root_length = strlen(root);

	node->data = host;
	node->next = hosts_root;
	hosts_root = node;
}

void set_request_host(struct http_request *request){
	static struct ll *hosts;

	hosts = hosts_root;
	while(hosts != NULL){
		static struct host *host;

		host = hosts->data;
		if(
			!strncasecmp(host->target_base, request->target, host->target_base_length)
		 && !strncasecmp(host->hostname, request->host_header->value, host->hostname_length)
		 && (
				(
					request->host_header->value[host->hostname_length] == '\0'
				 && !strcmp(host->port, "80")
				)
			 || (
					request->host_header->value[host->hostname_length] == ':'
				 && !strcmp(&request->host_header->value[host->hostname_length + 1], host->port)
				)
			)
		){
			request->host = host;
			return;
		}

		hosts = hosts->next;
	}
}



void* add_mem(unsigned long size){
	static struct ll *node;
	static struct mem_pool *mem;

	if(size > (1 << MAX_MEM_LEN_BITS))
		return NULL;

	node = malloc(sizeof(struct ll));
	if(node == NULL)
		exit(1);

	mem = malloc(sizeof(struct mem_pool));
	if(mem == NULL)
		exit(1);

	mem->mem = malloc(size);
	if(mem->mem == NULL)
		exit(1);

	node->data = mem;
	node->next = NULL;

	mem->in_use = 1;
	mem->size = size;
	mem->node = node;

	if(mem_pool_root == NULL)
		mem_pool_root = node;

	else{
		static struct ll *mem_ll;
		mem_ll = mem_pool_root;

		while(mem_ll->next != NULL){
			static struct mem_pool *mem_ll_mem;
			mem_ll_mem = mem_ll->next->data;

			if(mem_ll_mem->size >= size)
				break;

			mem_ll = mem_ll->next;
		}

		node->next = mem_ll->next;
		mem_ll->next = node;
	}

	return mem->mem;
}

void* get_mem(unsigned long size){
	static struct ll *node;

	node = mem_pool_root;
	while(node != NULL){
		static struct mem_pool *mem;

		mem = node->data;
		if(mem->size >= size && !mem->in_use){
			mem->in_use = 1;
			return mem->mem;
		}

		node = node->next;
	}

	return add_mem(size);
}

void free_mem(void *ptr){
	static struct ll *node;
	node = mem_pool_root;

	while(node != NULL){
		static struct mem_pool *mem;

		mem = node->data;
		if(mem->mem == ptr){
			mem->in_use = 0;
			return;
		}

		node = node->next;
	}
}

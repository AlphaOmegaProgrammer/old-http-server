#include <arpa/inet.h>
#include <netinet/in.h>
#include <poll.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include "global.h"



#define MAX_HEADERS_SIZE	4095



void close_request(struct http_request *request){
	static struct ll *header;

	header = request->headers;
	while(header != NULL){
		request->headers = header->next;
		free_mem(header->data);
		free_mem(header);
		header = request->headers;
	}

	free_mem(request->buffer);

	shutdown(request->socket_fd, SHUT_RDWR);
	close(request->socket_fd);
}

void respond(char *code, struct http_request *request){
	static char response[34] = "HTTP/1.1 000\r\nContent-Length:0\r\n\r\n";

	response[9] = code[0];
	response[10] = code[1];
	response[11] = code[2];

	send(request->socket_fd, response, 34, 0);
	close_request(request);
}



int main(){
#ifdef MODULE_SERVE_STATIC
	void serve_static_media_types();
	serve_static_media_types();
#endif

#ifdef MODULE_PROXY
	void proxy_media_types();
	proxy_media_types();
#endif

	add_host("127.0.0.1", "8080", "/", "/home/user/git/old-code-experiments/c/http-server/web-files/");

	unsigned char val = 1;
	int me = socket(AF_INET, SOCK_STREAM, 0);
	if(me < 0 || setsockopt(me, SOL_SOCKET, SO_REUSEADDR, &val, sizeof(int)) < 0){
		printf("Lol u suk\n");
		return 1;
	}

	struct sockaddr_in s = {0};
	s.sin_family = AF_INET;
	s.sin_port = htons(8080);
	inet_pton(AF_INET, "127.0.0.1", &s.sin_addr);
	if(bind(me, (struct sockaddr*)&s, sizeof s) < 0){
		printf("Error binding the socket\n");
		exit(1);
	}

	struct pollfd *fds;
	fds = malloc(sizeof(struct pollfd));
	fds[0].fd = me;
	fds[0].events = POLLIN;

	listen(me, 4);

	// Main loop
	for(;;){
		static struct http_request request;
		static size_t read_length;

main_loop_start:
		poll(fds, 1, -1);

		request.socket_fd = accept(me, NULL, NULL);
		if(request.socket_fd < 1)
			continue;

		request.responded = 0;
		request.response.code = 200;
		request.headers = NULL;
		request.host_header = NULL;
		request.host = NULL;
		request.media_type = NULL;

		read_length = recv(request.socket_fd, NULL, MAX_HEADERS_SIZE+1, MSG_PEEK | MSG_TRUNC);
		if(read_length < 1){
			close_request(&request);
			continue;
		}

		if(read_length > MAX_HEADERS_SIZE){
			read_length = MAX_HEADERS_SIZE;
			request.more_recv = 1;
		}else
			request.more_recv = 0;

		request.buffer = get_mem(read_length+1);

		recv(request.socket_fd, request.buffer, read_length, 0);
		request.method = request.buffer;
		request.buffer[read_length] = '\0';

		request.target = strchr(request.buffer, ' ');
		if(request.target == NULL){
			if(read_length == MAX_HEADERS_SIZE)
				respond("501", &request);
			else
				respond("400", &request);

			continue;
		}

		request.target[0] = '\0';
		request.target++;

		request.version = strchr(request.target, ' ');
		if(request.target == NULL){
			if(read_length == MAX_HEADERS_SIZE)
				respond("414", &request);
			else
				respond("400", &request);

			continue;
		}

		request.version[0] = '\0';
		request.version++;

		request.target_ext = strrchr(request.target, '.');
		if(request.target_ext != NULL)
			request.target_ext++;

		request.payload = strstr(request.version, "\r\n");
		if(request.payload == NULL){
			respond("400", &request);
			continue;
		}

		request.payload[0] = '\0';
		if(strchr(request.version, ' ') != NULL){
			respond("400", &request);
			continue;
		}

		while(memcmp(request.payload, "\0\n\r\n", 4) != 0){
			static char *value_space_ptr;
			static struct ll *node;
			static struct header *header;

			node = get_mem(sizeof(struct ll));
			header = get_mem(sizeof(struct header));

			node->data = header;
			node->next = request.headers;
			request.headers = node;

			header->name = request.payload + 2;
			header->value = strchr(request.payload + 2, ':');

			if(header->value == NULL || header->name == header->value || header->value[-1] == ' '){
				if(read_length == MAX_HEADERS_SIZE)
					respond("431", &request);
				else
					respond("400", &request);

				goto main_loop_start;
			}

			header->value[0] = '\0';
			header->value++;

			while(header->name[0] == ' ')
				header->name++;

			while(header->value[0] == ' ')
				header->value++;

			request.payload = strstr(header->value, "\r\n");
			if(request.payload == NULL || request.payload == header->value){
				if(read_length == MAX_HEADERS_SIZE)
					respond("431", &request);
				else
					respond("400", &request);

				continue;
			}

			request.payload[0] = '\0';

			value_space_ptr = strchr(header->value, ' ');
			if(value_space_ptr != NULL)
				value_space_ptr[0] = '\0';

			if(!strcasecmp(header->name, "host")){
				if(request.host_header != NULL){
					respond("400", &request);
					goto main_loop_start;
				}

				request.host_header = header;
			}
		}

		if(request.host_header == NULL){
			respond("400", &request);
			continue;
		}

		request.payload += 4;

		set_request_host(&request);

		if(request.host == NULL){
			respond("400", &request);
			continue;
		}

		if(request.target_ext != NULL)
			set_request_media_type(&request);


		if(request.media_type != NULL)
			request.media_type->handler(&request);

		if(!request.responded){
			if(!strcmp("GET", request.method) || !strcmp("HEAD", request.method))
				respond("404", &request);
			else
				respond("501", &request);
		}

		close_request(&request);
	}

	return 0;
}

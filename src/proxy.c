#include <arpa/inet.h> 
#include <netinet/in.h> 
#include <stdio.h> 
#include <string.h> 
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include "global.h"

void proxy(struct http_request *request){
	static int p;
	static struct sockaddr_in s;

	p = socket(AF_INET, SOCK_STREAM, 0);
	s.sin_family = AF_INET;
	s.sin_port = htons(9090);
	inet_pton(AF_INET, "127.0.0.1", &s.sin_addr);
	if(connect(p, (struct sockaddr*)&s, sizeof s) >= 0){
		static char *buffer;
		static char len_byte, send_header[8] = "\x01\x04\x00\x01\x00\x00\x00\x00", recv_header[8];
		static unsigned short rlen, rlen_tmp;

		send(p, "\x01\x01\x00\x01\x00\x08\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00", 16, 0);

		len_byte = 27 + request->host->root_length + strlen(request->target);

		send_header[4] = 0;
		send_header[5] = len_byte + 36;
		send(p, send_header, 8, 0);

		send(p, "\x0F", 1, 0);
		send(p, &len_byte, 1, 0);
		send(p, "SCRIPT_FILENAMEproxy:fcgi://127.0.0.1:9090", 42, 0);
		send(p, request->host->root, request->host->root_length, 0);
		send(p, request->target, strlen(request->target), 0);
		send(p, "\x0E\x03REQUEST_METHODGET", 19, 0);

		send(p, "\x01\x04\x00\x01\x00\x00\x00\x00\x01\x05\x00\x01\x00\x00\x00\x00", 16, 0);

		while(recv(p, recv_header, 8, 0) == 8){
			if(recv_header[1] == 3)
				break;

			if(recv_header[1] != 6){
				printf("Handle this correctly, idiot\n%x %x %x %x %x %x %x %x\n", recv_header[0], recv_header[1], recv_header[2], recv_header[3], recv_header[4], recv_header[5], recv_header[6], recv_header[7]);
				break;
			}

			if(!request->responded){
				send(request->socket_fd, "HTTP/1.1 200\r\n", 14, 0);
				request->responded = 1;
			}

			rlen = (((unsigned char)recv_header[4]) << 8) + (unsigned char)recv_header[5];
			if(!rlen)
				continue;

			buffer = get_mem(rlen);
			for(rlen_tmp = 0; rlen_tmp < rlen;)
				rlen_tmp += recv(p, buffer + rlen_tmp, rlen - rlen_tmp, 0);

			if(recv_header[6] > 0)
				recv(p, recv_header, recv_header[6], 0);

			for(rlen_tmp = 0; rlen_tmp < rlen;)
				rlen_tmp += send(request->socket_fd, buffer + rlen_tmp, rlen - rlen_tmp, 0);

			free_mem(buffer);
		}
	}else{
		send(request->socket_fd, "HTTP/1.1 503\r\nContent-Length:0\r\n\r\n", 34, 0);
		request->responded = 1;
	}

	shutdown(p, SHUT_RDWR);
	close(p);
}



void proxy_media_types(){
	add_media_type("php", "", &proxy);
}

struct ll{
	void* data;
	struct ll *next;
};



struct http_request;

struct media_type{
	char *ext, *type;
	void (*handler)(struct http_request *request);
};

struct host{
	unsigned short hostname_length, target_base_length, root_length;
	char *target_base, *hostname, *port, *root;
};

struct header{
	char *name, *value;
};

struct http_request{
	int socket_fd;
	char *buffer, *method, *target, *target_ext, *version, *payload;
	struct header *host_header;
	struct ll *headers;
	struct host *host;
	struct media_type *media_type;
	struct response{
		unsigned short code;
		struct ll *headers;
	} response;
	unsigned char responded:1;
	unsigned char more_recv:1;
};


void add_media_type(char *ext, char *type, void (*handler)(struct http_request *request));
void add_host(char *hostname, char *port, char *target_base, char *root);

void set_request_host(struct http_request *request);
void set_request_media_type(struct http_request *request);

void* get_mem(unsigned long size);
void free_mem(void* mem);

CC = gcc
CFLAGS = -O3 -march=native -xc -std=c11 -fstack-protector-all -Wall -Wextra -pedantic

all:  proxy serve-static build



proxy:
	$(CC) $(CFLAGS) -c src/proxy.c -o objs/proxy.o

serve-static:
	$(CC) $(CFLAGS) -c src/serve-static.c -o objs/serve-static.o



build:
	$(CC) $(CFLAGS) -c src/global.c -o objs/global.o

	$(eval D = $(shell for i in $$(ls objs); do echo "-DMODULE_$${i/-/_} " | cut -d. -f1 | tr a-z A-Z; done))

	$(CC) $(CFLAGS) -c $(D) -o objs/main.o src/main.c
	$(CC) -o bin/webserver objs/*.o

minimal:
	$(CC) $(CFLAGS) -c src/global.c -o objs/global.o
	$(CC) $(CFLAGS) -c src/main.c -o objs/main.o
	$(CC) -o bin/webserver objs/global.o objs/main.o

clean:
	$(RM) bin/*
	$(RM) objs/*
